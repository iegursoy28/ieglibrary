package space.iegrsy.ieglibrary;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.net.Uri;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.vision.barcode.Barcode;

import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import space.iegrsy.libieg.barcode.BarcodeCaptureActivity;

public class BarcodeTestActivity extends AppCompatActivity {
    private static final String TAG = BarcodeTestActivity.class.getSimpleName();

    private static final int REQUEST_BARCODE = 123;
    private static final int REQUEST_CAMERA_PERMISSION = 124;

    @BindView(R.id.barcode_txt)
    TextView captureTxt;

    @OnClick(R.id.barcode_btn)
    void captureBarcode() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.CAMERA},
                    REQUEST_CAMERA_PERMISSION);
        } else {
            Intent intent = new Intent(BarcodeTestActivity.this, BarcodeCaptureActivity.class);
            startActivityForResult(intent, REQUEST_BARCODE);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_barcode_test);

        ButterKnife.bind(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_BARCODE) {
            if (resultCode == CommonStatusCodes.SUCCESS) {
                if (data != null) {
                    Barcode barcode = (Barcode) data.getParcelableExtra(BarcodeCaptureActivity.BarcodeObject);
                    setCaptureTxt(String.format("Barcode: %s", barcode.displayValue));
                } else {
                    Toast.makeText(this, getString(R.string.error_code_not_correct), Toast.LENGTH_LONG).show();
                }
            } else {
                Log.e(TAG, String.format(getString(R.string.barcode_error_format),
                        CommonStatusCodes.getStatusCodeString(resultCode)));
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CAMERA_PERMISSION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    captureBarcode();
                } else {
                    Snackbar.make(
                            findViewById(android.R.id.content),
                            "Need camera permission.",
                            Snackbar.LENGTH_SHORT)
                            .setAction("GRANT", new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                    Uri uri = Uri.fromParts("package", getPackageName(), null);
                                    intent.setData(uri);
                                    startActivityForResult(intent, REQUEST_CAMERA_PERMISSION);
                                }
                            }).show();
                }
            }
        }
    }

    private void setCaptureTxt(final String s) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                captureTxt.setText(s);
            }
        });
    }
}
