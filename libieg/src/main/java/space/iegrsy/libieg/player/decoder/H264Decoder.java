package space.iegrsy.libieg.player.decoder;

import android.media.MediaCodec;
import android.media.MediaFormat;
import android.util.Log;
import android.view.Surface;

import java.io.IOException;
import java.nio.ByteBuffer;

public class H264Decoder extends Thread {
    private static final String TAG = "H264Decoder";

    private static final String MIME_TYPE = "video/avc";
    private static final int FRAME_RATE = 12;
    private static final int TIMEOUT_USEC = 1000;

    private static int count = 0;

    private int MediaCodecWidth = 1920;
    private int MediaCodecHeight = 1080;

    private MediaCodec decoder;

    private final StreamData mData;
    private Surface surface;

    private boolean isRun;

    public H264Decoder(StreamData mData, Surface surface) {
        this.mData = mData;
        this.surface = surface;
    }

    @Override
    public void run() {
        isRun = true;
        while (mData.getHeader_sps() == null || mData.getHeader_pps() == null || mData.getFrames().size() <= 0) {
            if (!isRun)
                break;

            try {
                sleep(300);
                Log.i(TAG, "Waiting...");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        prepareDecoder();

        if (decoder == null) {
            return;
        }

        ByteBuffer[] inputBuffers;
        ByteBuffer[] outputBuffers;
        try {
            inputBuffers = decoder.getInputBuffers();
            outputBuffers = decoder.getOutputBuffers();
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }

        int frameIndex = 0;
        int frameSize = 0;
        RAWFrame frame;

        byte[] outData = null;
        byte[] data = null;
        long lastts = 0;

        count++;
        int threadCount = count;
        while (isRun) {
            if (mData.getFrames().size() <= 0)
                continue;

            frame = mData.getFrame(frameIndex);
            frameSize = frame.frameData.length;

            if (data == null || data.length < frameSize)
                data = new byte[frameSize];

            System.arraycopy(frame.frameData, 0, data, 0, frameSize);
            long lts = System.currentTimeMillis() - frame.ts;
            //Log.d(TAG, threadCount + ": Lag ts: " + lts);

            mData.removeFrame(frameIndex);

            int inIndex = -1;
            while ((inIndex = decoder.dequeueInputBuffer(TIMEOUT_USEC)) < 0)
                if (!isRun)
                    break;

            if (inIndex >= 0) {
                if (frameSize < 0) {
                    Log.d(TAG, "End of stream");
                    decoder.queueInputBuffer(inIndex, 0, 0, 0, MediaCodec.BUFFER_FLAG_END_OF_STREAM);
                    break;
                } else {
                    ByteBuffer buffer = inputBuffers[inIndex];
                    buffer.clear();
                    buffer.put(data);
                    decoder.queueInputBuffer(inIndex, 0, frameSize, 0, MediaCodec.CRYPTO_MODE_UNENCRYPTED);
                }

                MediaCodec.BufferInfo info = new MediaCodec.BufferInfo();
                int outIndex = decoder.dequeueOutputBuffer(info, TIMEOUT_USEC);
                switch (outIndex) {
                    case MediaCodec.INFO_OUTPUT_BUFFERS_CHANGED:
                        Log.d(TAG, "INFO_OUTPUT_BUFFERS_CHANGED");
                        outputBuffers = decoder.getOutputBuffers();
                        break;
                    case MediaCodec.INFO_OUTPUT_FORMAT_CHANGED:
                        Log.d(TAG, "INFO_OUTPUT_FORMAT_CHANGED: New format: " + decoder.getOutputFormat());
                        break;
                    case MediaCodec.INFO_TRY_AGAIN_LATER:
                        Log.d(TAG, "INFO_TRY_AGAIN_LATER");
                        try {
                            Thread.sleep(10);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        break;
                    default:
                        ByteBuffer outbuffer = outputBuffers[outIndex];
                        if (outData == null) {
                            outData = new byte[info.size];
                        }
                        outbuffer.get(outData);
                        decoder.releaseOutputBuffer(outIndex, true);
                        if (lastts > 0) {
                            long diff = System.currentTimeMillis() - lastts;
                            while (diff < 80) {
                                try {
                                    Log.d(TAG, "Sleeping...");
                                    Thread.sleep(10);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                                diff = System.currentTimeMillis() - lastts;
                            }
                            lastts = System.currentTimeMillis();
                        }
                        break;
                }
            }
        }

        decoder.setParameters(null);
        decoder.stop();
        decoder.release();
        decoder = null;
    }

    private void prepareDecoder() {
        MediaFormat mediaFormat = MediaFormat.createVideoFormat(MIME_TYPE, MediaCodecWidth, MediaCodecHeight);
        mediaFormat.setByteBuffer("csd-0", ByteBuffer.wrap(mData.getHeader_sps()));
        mediaFormat.setByteBuffer("csd-1", ByteBuffer.wrap(mData.getHeader_pps()));
        mediaFormat.setInteger(MediaFormat.KEY_FRAME_RATE, FRAME_RATE);

        try {
            decoder = MediaCodec.createDecoderByType(MIME_TYPE);
            decoder.configure(mediaFormat, surface, null, 0);
            decoder.start();
        } catch (IOException | IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

    public void release() {
        Log.d(TAG, "releasing encoder objects");
        isRun = false;
    }
}
