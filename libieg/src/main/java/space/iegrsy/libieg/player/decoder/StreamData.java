package space.iegrsy.libieg.player.decoder;

import java.util.ArrayList;

public class StreamData {
    private int frameID;
    private ArrayList<RAWFrame> frames;

    private byte[] header_sps;
    private byte[] header_pps;

    public StreamData() {
        frameID = 0;
        frames = new ArrayList<RAWFrame>();
        header_sps = null;
        header_pps = null;
    }

    public synchronized void incrementFrameID() {
        frameID++;
    }

    public synchronized int getFrameID() {
        return frameID;
    }

    public synchronized void addFrame(RAWFrame frame) {
        if (frame != null)
            frames.add(frame);
    }

    public synchronized RAWFrame getFrame(int index) {
        return frames.get(index);
    }

    public synchronized void removeFrame(int index) {
        if (0 <= index && index < frames.size())
            frames.remove(index);
    }

    public synchronized ArrayList<RAWFrame> getFrames() {
        return frames;
    }

    public synchronized void setHeader_sps(byte[] header_sps) {
        if (this.header_sps == null) {
            this.header_sps = new byte[header_sps.length];
            System.arraycopy(header_sps, 0, this.header_sps, 0, header_sps.length);
        }
    }

    public synchronized void setHeader_pps(byte[] header_pps) {
        if (this.header_pps == null) {
            this.header_pps = new byte[header_pps.length];
            System.arraycopy(header_pps, 0, this.header_pps, 0, header_pps.length);
        }
    }

    public synchronized byte[] getHeader_sps() {
        return header_sps;
    }

    public synchronized byte[] getHeader_pps() {
        return header_pps;
    }
}
