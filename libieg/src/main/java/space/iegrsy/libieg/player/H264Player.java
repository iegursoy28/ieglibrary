package space.iegrsy.libieg.player;

import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import space.iegrsy.libieg.player.decoder.H264Decoder;
import space.iegrsy.libieg.player.decoder.RAWFrame;
import space.iegrsy.libieg.player.decoder.StreamData;

public class H264Player implements SurfaceHolder.Callback {
    private static final String TAG = "H264Player";

    private final SurfaceView surfaceView;
    private StreamData mData = null;
    private H264Decoder mPlayer;

    public H264Player(SurfaceView surfaceView) {
        this.surfaceView = surfaceView;
        this.surfaceView.getHolder().addCallback(this);
        mData = new StreamData();
    }

    public int useFrameData(byte[] readInData) {
        int off = 0;
        if (readInData[off] != 0
                || readInData[off + 1] != 0
                || readInData[off + 2] != 0
                || readInData[off + 3] != 1)
            return -1;

        if (readInData[off + 4] == 0x67) {
            Log.v(TAG, "SPS encounter");
            mData.setHeader_sps(readInData);
        } else if (readInData[off + 4] == 0x68) {
            Log.v(TAG, "PPS encounter");
            mData.setHeader_pps(readInData);
        }

        if (readInData.length > 300) { //TODO: hack
            //Log.v(TAG, "New frame with size " + readInData.length);
            RAWFrame frame = new RAWFrame(mData.getFrameID());
            frame.frameData = new byte[readInData.length];
            System.arraycopy(readInData, 0, frame.frameData, 0, readInData.length);
            frame.ts = 80; //buffer.getTs();
            mData.addFrame(frame);
            mData.incrementFrameID();
        }

        return readInData.length;
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        Log.d(TAG, "Surface created.");
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        if (mPlayer == null && holder.getSurface() != null) {
            mPlayer = new H264Decoder(mData, holder.getSurface());
            mPlayer.start();
        }
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        if (mPlayer != null) {
            mPlayer.release();
            mPlayer = null;
        }
    }
}